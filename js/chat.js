var numberMsgPeople = 0;
var numberMsgBot = 0;
var numberQuestion = 1;
var opc = 0;
var nome, porte, dataAgendamento, horaAgendamento, cor, raca, endereco, dataBr, casoPet;
var primeiraMsg;

$('#btnEnviar').click(function () {
    msgPeople();
});

$("#msgDigite").on("keyup", function (e) {
    if (e.which == 13) {
        msgPeople()
    }
});

function msgPeople() {
    var msg = $("#msgDigite").val();
    if (msg != '' && msg != undefined) {
        var $clone = $(".mensagemPeople").clone().appendTo(".msg_history").prop('id', 'msg_' + numberMsgPeople);

        $clone.find('.msgPeople').html(msg);
        $clone.removeClass('d-none');
        $clone.removeClass('mensagemPeople');
        $clone.addClass('msgConversa');

        respPeople();
        $("#msgDigite").val('');
        numberMsgPeople++;

        var objDiv = document.getElementById("scroll");
        objDiv.scrollTop = objDiv.scrollHeight;
    }
}

function msgBot(msg, erro) {
    var $clone = $(".mensagemBot").clone().appendTo(".msg_history").prop('id', 'msg_' + numberMsgBot);

    $clone.find('.msgBot').html(msg);
    $clone.removeClass('d-none');
    $clone.removeClass('mensagemBot');
    $clone.addClass('msgConversa');

    if (!erro) {
        numberMsgBot++;
    }

    var objDiv = document.getElementById("scroll");
    objDiv.scrollTop = objDiv.scrollHeight;

    if (primeiraMsg) {
        primeiraMsg = false;
    } else {
        setTimeout(function () { limparChat() }, 120000);
    }

}

function msgInicial() {
    primeiraMsg = true;
    var data = new Date();
    var horas = addZero(data.getHours());
    var minutos = addZero(data.getMinutes());
    var horario = horas + ":" + minutos;
    var saudacao = '';

    if (horario >= "06:00" && horario <= "11:59") {
        saudacao = "Bom Dia,";
    } else if (horario >= "12:00" && horario <= "17:59") {
        saudacao = "Boa Tarde, ";
    } else {
        saudacao = "Boa Noite, ";
    }

    var msg = saudacao + "<br>Escolha uma Opção abaixo:<br>1-	Agendamento de Banho e Tosa<br>2-   Cadastrar Cachorro Perdido ou Encontrado"
    msgBot(msg, false);
}

function msgFinal() {
    var data = new Date();
    var horas = addZero(data.getHours());
    var minutos = addZero(data.getMinutes());
    var horario = horas + ":" + minutos;
    var saudacao = '';

    if (horario >= "06:00" && horario <= "11:59") {
        saudacao = "Bom Dia,";
    } else if (horario >= "12:00" && horario <= "17:59") {
        saudacao = "Boa Tarde, ";
    } else {
        saudacao = "Boa Noite, ";
    }

    var msg = saudacao + "<br>Obrigado por usar o Click Pet.<br>Volte Sempre!!";
    msgBot(msg, false);
    setTimeout(function () { limparChat() }, 30000);
}

function respPeople() {
    var msg = $("#msgDigite").val();
    if (msg != '' && msg != undefined) {
        switch (numberMsgBot) {
            case 1:
                if (msg == 1 || msg == 2) {
                    opc = msg;
                    getQuestion(false);
                } else {
                    msgBot("Opção Inválida<br>Escolha uma Opção abaixo:<br>1-	Agendamento de Banho e Tosa<br>2-   Cadastrar Cachorro Perdido ou Encontrado", true);
                }
                break;
            case 2:
                if (opc == 1) {
                    nome = msg;
                    getQuestion(false);
                } else {
                    if (msg == 1 || msg == 2) {
                        casoPet = msg
                        getQuestion(false);
                    } else {
                        msgBot("Opção Inválida", true);
                        numberQuestion = 1;
                        getQuestion(true);
                    }
                }
                break;
            case 3:
                if (opc == 1) {
                    if (msg == 1 || msg == 2 || msg == 3) {
                        porte = msg;
                        getQuestion(false);
                    } else {
                        msgBot("Opção Inválida", true);
                        numberQuestion = 2;
                        getQuestion(true);
                    }
                } else {
                    nome = msg;
                    getQuestion(false);
                }
                break;
            case 4:
                if (opc == 1) {
                    raca = msg;
                    getQuestion(false);
                } else {
                    cor = msg;
                    getQuestion(false);
                }
                break;
            case 5:
                if (opc == 1) {
                    dataBr = msg;
                    var dataTemp = msg.split('/');
                    dataAgendamento = "2021-" + dataTemp[0] + "-" + dataTemp[1];
                    getQuestion(false);
                } else {
                    raca = msg;
                    getQuestion(false);
                }
                break;
            case 6:
                if (opc == 1) {
                    horaAgendamento = msg;
                    salvarAgendamento();
                } else {
                    endereco = msg;
                    getQuestion(false);
                }
                break;
            case 7:
                if (opc == 1) {
                    if (msg == "S") {
                        numberMsgBot = 0;
                        numberQuestion = 1;
                        msgInicial();
                    } else if (msg == "N") {
                        msgFinal();
                    }
                } else {
                    if (msg == 1 || msg == 2 || msg == 3) {
                        porte = msg;
                        salvarPet();
                    } else {
                        msgBot("Opção Inválida", true);
                        numberQuestion = 6;
                        getQuestion(true);
                    }
                }
                break;
            case 8:
                if (opc == 2) {
                    if (msg == "S") {
                        numberMsgBot = 0;
                        numberQuestion = 1;
                        msgInicial();
                    } else if (msg == "N") {
                        msgFinal();
                    }
                }
            default:
                console.log("Erro");
                break;
        }
    }
}

function limparChat() {
    $(".msgConversa").remove();
    numberMsgPeople, opc, numberMsgBot = 0;
    numberQuestion = 1;
    nome, porte, dataAgendamento, horaAgendamento, cor, raca, endereco, dataBr, casoPet = "";
    msgInicial();
}

function addZero(i) {
    if (i < 10) { i = "0" + i }
    return i;
}

function salvarAgendamento() {

    var body;

    body = '{"acao": "Agendamento", "nome": "' + nome + '", "porte": "' + porte + '", "raca": "' + raca + '", "dataAgendamento": "' + dataAgendamento + '", "horaAgendamento": "' + horaAgendamento + '"}';
    $.ajax({
        type: "POST",
        url: "api/fn_agendamento.php",
        data: body,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        success: function (dados) {
            var msg = "Banho/Tosa agendada com sucesso!!<br>Dia " + dataBr + " às " + horaAgendamento + "<br>Senha: " + dados[0].senha + "<br><br>Posso te ajudar em algo mais?(S/N)";
            msgBot(msg);
        },
        error: function (x, e) {
            alert("erro")
        }
    })
}

function salvarPet() {

    var body;

    body = '{"acao": "Pet", "nome": "' + nome + '", "cor": "' + cor + '", "raca": "' + raca + '", "endereco": "' + endereco + '", "porte": "' + porte + '", "casoPet": "' + casoPet + '"}';
    $.ajax({
        type: "POST",
        url: "api/fn_pet.php",
        data: body,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        success: function (dados) {
            var msg = ""
            if (casoPet == 1) {
                msg = "Cadastro de Animal Encontrado Efetuado!<br><br>Posso te ajudar em algo mais?(S/N)";
            } else {
                msg = "Cadastro de Animal Perdido Efetuado!<br><br>Posso te ajudar em algo mais?(S/N)";
            }
            msgBot(msg);
        },
        error: function (x, e) {
            alert("erro")
        }
    })
}

function getQuestion(opcInvalida) {

    var body;

    body = '{"acao": "Question", "question": "' + numberQuestion + '", "opc": "' + opc + '"}';
    $.ajax({
        type: "POST",
        url: "api/fn_chat.php",
        data: body,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        success: function (dados) {
            msgBot(dados[0].dados[0].mensagem, opcInvalida);
            numberQuestion++;
        },
        error: function (x, e) {
            alert("erro")
        }
    })
}

$(document).ready(function () {
    msgInicial();
});