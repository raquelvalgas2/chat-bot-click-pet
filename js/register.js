$('#bt_cadastrar').click(function () {
    var p_usuario = $("#inputUsuario").val();
    var p_email = $("#inputEmail").val();
    var p_senha = $("#inputSenha").val();
    var p_Csenha = $("#inputCSenha").val();
    var p_telefone = $("#inputTelefone").val();
    if (p_senha == p_Csenha) {
        if (p_usuario != '' && p_email != '' && p_senha != '' && p_telefone != '') {
            fn_inserir(p_usuario, p_email, p_senha, p_telefone);
            $('#bt_cadastrar').html("<span class= 'spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>Cadastrando...");
            $('#bt_cadastrar').attr("disabled", true);
        } else {
            fn_sucesso("Campo Vazio", "Nenhum campo pode estar vazio");
        }
    } else {
        fn_sucesso("Senha errada", "Confirmação de senha e senha tem que ser iguais.");
    }
});

$('#bt_entrar').click(function () {
    var p_email = $("#inputEmail").val();
    var p_senha = $("#inputSenha").val();
    if (p_email != '' && p_senha != '') {
        fn_acesso(p_email, p_senha);
        $('#bt_entrar').html("<span class= 'spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>Entrando...");
        $('#bt_entrar').attr("disabled", true);
    } else {
        fn_sucesso("Campo Vazio", "Nenhum campo pode estar vazio");
    }
})

function fn_inserir(usuario, email, senha, telefone) {
    var body;

    body = '{"acao": "C", "nome": "' + usuario + '","email": "' + email + '", "senha": "' + senha + '", "telefone": "' + telefone + '"}';
    $.ajax({
        type: "POST",
        url: "api/fn_register.php",
        data: body,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        success: function (dados) {
            if (dados.info[0].cod == '200') {
                $('#inputUsuario, #inputEmail, #inputSenha, #inputCSenha, #inputTelefone').val('');
                $('#bt_cadastrar').html("Cadastrar");
                $('#bt_cadastrar').attr("disabled", false);
                window.location.href = "/chat-bot-click-pet/chat.php";
            } else if (dados.info[0].cod == '406') {
                fn_sucesso("Campo", 'Existe campo vazio');
                $('#bt_cadastrar').html("Cadastrar");
                $('#bt_cadastrar').attr("disabled", false);
            } else if (dados.info[0].cod == '401') {
                fn_sucesso("Email", 'Já existe email cadastrado');
                $('#bt_cadastrar').html("Cadastrar");
                $('#bt_cadastrar').attr("disabled", false);
            }
        },
        error: function (x, e) {
            alert("erro")
        }
    })
}

function fn_acesso(email, senha) {
    var body;

    body = '{"acao": "Entrar", "email": "' + email + '", "senha": "' + senha + '"}';
    $.ajax({
        type: "POST",
        url: "api/fn_register.php",
        data: body,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        success: function (dados) {
            if (dados.info[0].cod == '200') {
                $('#inputEmail, #inputSenha').val('');
                $('#bt_entrar').html("Entrar");
                $('#bt_entrar').attr("disabled", false);
                window.location.href = "/chat-bot-click-pet/chat.php";
            } else if (dados.info[0].cod == '406') {
                fn_sucesso("Campo", 'Existe campo vazio');
                $('#bt_entrar').html("Entrar");
                $('#bt_entrar').attr("disabled", false);
            } else if (dados.info[0].cod == '401') {
                fn_sucesso("Inválido", 'Senha ou E-mail inválido.');
                $('#bt_entrar').html("Entrar");
                $('#bt_entrar').attr("disabled", false);
            }
        },
        error: function (x, e) {
            alert("erro")
        }
    })
}

function fn_sucesso(titulo, msg) {
    $('#Modal_Info').modal('show');
    $('#Titulo_Modal_Info').html(titulo);
    $('#Body_Modal_Info').html(msg);
}

$(document).ready(function () {
    $("#inputTelefone").mask("(00)00000-0000");
});