<!doctype html>

<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Click Pet</title>
    <link rel="stylesheet" type="text/css" href="css/chat.css" />
    <link href="https://fonts.googleapis.com/css?family=Coiny|Knewave|Pacifico|Titan+One&display=swap" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/base.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <div class="container">
        <a href="#"><img src="img/adcursor.png" class="log"></a>
        <header class="header">
            <h5 class="mt-2 click">ClickPet</h5>
        </header>

        <div class="messaging mt-2">
            <div class="inbox_msg">

                <div class="mesgs">
                    <div id="scroll" class="msg_history overflow-auto">
                        <div class="incoming_msg mensagemBot d-none">
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p class="msgBot">Test which is a new approach to have all
                                        solutions</p>
                                </div>
                            </div>
                        </div>
                        <div class="outgoing_msg mensagemPeople d-none">
                            <div class="sent_msg">
                                <p class="msgPeople">Test which is a new approach to have all
                                    solutions</p>
                            </div>
                        </div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" id="msgDigite" placeholder="Escreva a Mensagem" />
                            <button id="btnEnviar" class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="js/pet.js"></script>
    <script src="js/chat.js"></script>
</body>

</html>