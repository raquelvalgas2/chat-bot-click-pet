<!doctype html>

<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Click Pet</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/base.css" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

</head>

<body>
    <div class="d-flex align-items-center min-vh-100">
        <div class="container c_login border border-warning b_login">
            <div class="row">
                <div class="col">
                    <img src="img/adcursor.png" alt="Click Pet" class="mt-5 mx-auto d-block " style="width:10rem">
                </div>
            </div>
            <form action="">
                <div class="form-row">
                    <div class="col from-group">
                        <label>Usuário</label>
                        <input id="inputUsuario" type="text" class="form-control" placeholder="Digite seu usuário">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col from-group">
                        <label>Telefone</label>
                        <input id="inputTelefone" type="text" class="form-control" placeholder="Digite seu Telefone ou Celular">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col from-group">
                        <label>E-mail</label>
                        <input id="inputEmail" type="text" class="form-control" placeholder="Digite seu e-mail">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col from-group">
                        <label>Senha</label>
                        <input id="inputSenha" type="password" class="form-control" placeholder="Digite sua senha">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col from-group">
                        <label>Confirme sua senha</label>
                        <input id="inputCSenha" type="password" class="form-control" placeholder="Confirme sua senha">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <button id='bt_cadastrar' type='button' class="mb-4 btn btn-primary m-2 mx-auto d-block">Cadastrar</button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <a href="index.php">Voltar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- modal de info -->

    <div class="modal fade" id="Modal_Info" tabindex="-1" role="dialog" aria-labelledby="Modal_Info" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="Titulo_Modal_Info">?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="Body_Modal_Info" class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="js/register.js"></script>
</body>

</html>