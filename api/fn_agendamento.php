<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include 'class_agendamento.php';
include 'util_post.php';


$acao = $data['acao'];
$agendamento = new agendamento();
switch ($acao) {
    case 'Agendamento':
        $nome = $data['nome'];
        $porte = $data['porte'];
        $raca = $data['raca'];
        $dataAgendamento = $data['dataAgendamento'];
        $horaAgendamento = $data['horaAgendamento'];

        $numsenha = $agendamento->senha();
        $senha = $numsenha[0]["text"] . $numsenha[0]["num"];

        $retorno = $agendamento->create($nome, $porte, $raca, $dataAgendamento, $horaAgendamento, $senha);

        $agendamento->addsenha($numsenha[0]["num"] + 1);

        $myArray_final[] = array(
            "senha" => $senha
        );
        break;
}

echo json_encode($myArray_final);
