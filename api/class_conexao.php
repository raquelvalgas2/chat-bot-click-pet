<?php

class CONEXAO
{

    public $usuario;
    public $senha;
    public $sid;
    public $banco;
    public $mysqli;


    function __construct()
    {
        $this->sid     = "clickpet.cxk1adk7lted.us-east-2.rds.amazonaws.com";
        $this->usuario = "admin";
        $this->banco   = "chatbot";
        $this->senha   = "pipoca123456";
    }

    function Conecta()
    {
        $this->mysqli = new mysqli($this->sid, $this->usuario, $this->senha, $this->banco);
        $this->mysqli->set_charset('utf8');

        if ($this->mysqli->connect_error) {
            return  die("Connection failed: " . $this->mysqli->connect_error);
        }
        "Connected successfully";
    }

    function Desconecta()
    {
        return mysqli_close($this->mysqli);
    }

    function Consulta($consulta)
    {
        $this->Conecta($consulta);

        if ($retorno = $this->mysqli->query($consulta)) {
            return $retorno->fetch_all(MYSQLI_ASSOC);
            $this->Desconecta();
        } else {
            $this->Desconecta();
            die('Invalid query: ' . $consulta . "/n" . mysqli_error($this->mysqli));
        }
    }

    function Consulta_um($consulta, $campo)
    {
        $this->Conecta();
        //retorna o valor de um unico campo e linha
        // usar em count, true ou false somente
        if ($result = $this->mysqli->query($consulta)) {

            $row = $result->fetch_array();
            $this->Desconecta();
            return $row[$campo];
        } else {
            $this->Desconecta();
            die('Invalid query: ' . $consulta . "/n" . mysqli_error($this->mysqli));
        }
    }

    function inserir($consulta)
    {
        $this->Conecta();

        if ($retorno = $this->mysqli->query($consulta)) {
            $this->Desconecta();
            return $retorno;
        } else {
            return false;
        }
    }

    function update($consulta)
    {
        $this->Conecta();

        if ($this->mysqli->query($consulta)) {

            $retorno =  $this->mysqli->affected_rows;
            $this->Desconecta();
            return  $retorno;
        }
    }

    function delete($consulta)
    {
        $this->Conecta();

        if ($this->mysqli->query($consulta)) {
            $retorno = $this->mysqli->affected_rows;
            $this->Desconecta();
            return $retorno;
        }
    }
}
