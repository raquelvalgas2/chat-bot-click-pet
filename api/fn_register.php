<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include 'class_register.php';
include 'util_post.php';


$acao = $data['acao'];
$register = new register();
switch ($acao) {
    case 'C':
        $nome = $data['nome'];
        $email = $data['email'];
        $senha = $data['senha'];
        $telefone = $data['telefone'];
        $retornoEx = $register->checkExistente($email);
        if ($retornoEx == 0) {
            if ($nome != '' && $email != '' && $senha != '' && $telefone != '') {
                $retorno = $register->incluir($nome, $email, $senha, $telefone);
                $myArray_final["info"][] = array(
                    "cod" => '200'
                );
            } else {
                $myArray_final["info"][] = array(
                    "cod" => '406' // Campo vazio
                );
            }
        } else {
            $myArray_final["info"][] = array(
                "cod" => '401' // Já existe email
            );
        }
        break;
    case 'Entrar':
        $email = $data['email'];
        $senha = $data['senha'];
        if ($email != '' && $senha != '') {
            $retorno = $register->entrar($email, $senha);
            if ($retorno == 1) {
                $myArray_final["info"][] = array(
                    "cod" => '200'
                );
            } else {
                $myArray_final["info"][] = array(
                    "cod" => '401' // ja existe
                );
            }
        } else {
            $myArray_final["info"][] = array(
                "cod" => '406' // Campo vazio
            );
        }
        break;
}

echo json_encode($myArray_final);
